// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.


const inventory = require("./inventory.cjs");

function problem5 (inventory, year) {
    if (arguments.length < 1 || inventory.length < 2 || !Array.isArray(inventory) || typeof year !== "number") {
        return [];
    }
    
    let carsYearOlder = [];

    inventory.filter(row => { 
        if (row.car_year < year) {
            carsYearOlder.push(row.car_year)
        }
    });
    
    return carsYearOlder;
}

module.exports = problem5;