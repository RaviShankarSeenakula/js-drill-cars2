// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

const inventory = require("./inventory.cjs");

function problem6 (inventory, car1, car2) {
    if (arguments.length < 2 || inventory.length < 1 || !Array.isArray(inventory) || (typeof car1 !== "string" && typeof car2 !== "string")) {
        return [];
    }

    let cars = [];

    inventory.filter(row => {
        let car = row.car_make;
        if (car === car1 || car === car2) {
            cars.push(row);
        }
    });

    return JSON.stringify(cars);
}

module.exports = problem6;