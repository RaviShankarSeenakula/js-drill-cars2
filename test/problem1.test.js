const problem1 = require("../problem1.cjs")
const inventory = require("../inventory.cjs")

test('problem1 testing',() => {
    expect(problem1(inventory, 33)).toStrictEqual({ id: 33, car_make: 'Jeep', car_model: 'Wrangler', car_year: 2011 })
})

test('problem1 testing',() => {
    expect(problem1(inventory, 33, 45)).toStrictEqual({ id: 33, car_make: 'Jeep', car_model: 'Wrangler', car_year: 2011 })
})

test('problem1 testing',() => {
    expect(problem1(inventory)).toStrictEqual([])
})

test('problem1 testing',() => {
    expect(problem1()).toStrictEqual([])
})

test('problem1 testing',() => {
    expect(problem1('inventory',33)).toStrictEqual([])
})

test('problem1 testing',() => {
    expect(problem1(inventory,'33')).toStrictEqual([])
})

test('problem1 testing',() => {
    expect(problem1([],33)).toStrictEqual([])
})

test('problem1 testing',() => {
    expect(problem1(inventory,[])).toStrictEqual([])
})

test('problem1 testing',() => {
    expect(problem1(inventory,101)).toStrictEqual([])
})