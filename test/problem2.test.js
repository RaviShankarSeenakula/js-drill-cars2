const problem = require("../problem2.cjs")
const inventory = require("../inventory.cjs")

test('problem2 testing',() => {
    expect(problem(inventory)).toStrictEqual({ id: 50, car_make: 'Lincoln', car_model: 'Town Car', car_year: 1999 })
})

test('problem2 testing',() => {
    expect(problem(inventory, 33, 45)).toStrictEqual({ id: 50, car_make: 'Lincoln', car_model: 'Town Car', car_year: 1999 })
})

test('problem2 testing',() => {
    expect(problem({})).toStrictEqual([])
})

test('problem2 testing',() => {
    expect(problem()).toStrictEqual([])
})

test('problem2 testing',() => {
    expect(problem('inventory')).toStrictEqual([])
})

test('problem2 testing',() => {
    expect(problem([])).toStrictEqual([])
})
