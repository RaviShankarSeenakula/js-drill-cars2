const problem = require("../problem5.cjs")
const inventory = require("../inventory.cjs")

test('problem5 testing',() => {
    expect(problem(inventory, 2000)).toStrictEqual([
        1983, 1990, 1995, 1987, 1996,
        1997, 1999, 1987, 1995, 1994,
        1985, 1997, 1992, 1993, 1964,
        1999, 1991, 1997, 1992, 1998,
        1965, 1996, 1995, 1996, 1999
      ])
})

test('problem5 testing',() => {
    expect(problem(inventory, 2000, 2005)).toStrictEqual([
        1983, 1990, 1995, 1987, 1996,
        1997, 1999, 1987, 1995, 1994,
        1985, 1997, 1992, 1993, 1964,
        1999, 1991, 1997, 1992, 1998,
        1965, 1996, 1995, 1996, 1999
      ])
})

test('problem5 testing',() => {
    expect(problem({})).toStrictEqual([])
})

test('problem5 testing',() => {
    expect(problem()).toStrictEqual([])
})

test('problem5 testing',() => {
    expect(problem(inventory)).toStrictEqual([])
})

test('problem5 testing',() => {
    expect(problem(inventory, '2000')).toStrictEqual([])
})

test('problem5 testing',() => {
    expect(problem([])).toStrictEqual([])
})
